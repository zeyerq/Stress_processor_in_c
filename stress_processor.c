#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

// compile:
// gcc stress_processor.c -o stress_processor -pthread

// use:
// ./stress_processor --threads 4 --intensity 9


void* stress_processor(void* arg) {
    double result = 0.0;
    int intensity = *((int*)arg);

    while (1) {
        for (int i = 0; i < intensity * 100000; ++i) {
            result += i * i;
        }
    }

    return NULL;
}

int main(int argc, char* argv[]) {
    int num_threads = 1;
    int intensity = 1;

    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "--threads") == 0 && i + 1 < argc) {
            num_threads = atoi(argv[i + 1]);
        } else if (strcmp(argv[i], "--intensity") == 0 && i + 1 < argc) {
            intensity = atoi(argv[i + 1]);
        }
    }

    pthread_t* threads = (pthread_t*)malloc(num_threads * sizeof(pthread_t));
    if (threads == NULL) {
        perror("Error allocating memory for threads.");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < num_threads; ++i) {
        if (pthread_create(&threads[i], NULL, stress_processor, &intensity) != 0) {
            perror("Error creating thread.");
            exit(EXIT_FAILURE);
        }
    }

    printf("Stress of the processor running with %d thread(s) and intensity %d.\n", num_threads, intensity);

    while (1) {
        sleep(1);
    }

    for (int i = 0; i < num_threads; ++i) {
        pthread_join(threads[i], NULL);
    }

    free(threads);

    return 0;
}
